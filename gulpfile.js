/* Dependencias */
var gulp = require('gulp'),
	stylus = require('gulp-stylus'),
	nib = require('nib'),
	watch = require('gulp-watch'),
	uglify = require('gulp-uglify'),
	cleancss = require('gulp-clean-css'),
	imagemin    = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	jpegoptim = require('imagemin-jpegoptim'),
	newer = require('gulp-newer'),
	browserify = require('browserify'),
	source= require('vinyl-source-stream'),
	buffer= require('vinyl-buffer'),
	watchify = require('watchify');

var path = {};
path.js =  {
	main: './src/js/myapp.js',
	out: './dist/js/'
};
path.css = {
	main: './src/css/style.styl',
	components: './src/css/mycomponents/**/*.{css,styl}',
	vendor: './src/css/vendor/**/*.{css,styl}',
	out: './dist/css/'
};
path.img = {
	in: './src/img/**/*.{gif,png,jpg,svg,jpeg}',
	out: './dist/img/'
};
var fname = {};
fname.js = 'app.js';

function compile (watch) {
	var bundle = browserify(path.js.main, {
		debug: false
	});

	if (watch) {
		bundle = watchify(bundle);
		bundle.on('update', function () {
			console.log('--> Bundling.. ' + new Date().toLocaleTimeString());
			rebundle();
		});
	}

	function rebundle () {
		bundle/*
			.transform(babel, {
				presets: ['es2015'],
				plugins: ['syntax-async-functions', 'transform-regenerator']
			})*/
			.bundle()
			.on('error', function (err) {
				console.log(err);
				this.emit('end')
			})
			.pipe(source(fname.js))
			//.pipe(buffer())
			//.pipe(uglify())
			.pipe(gulp.dest(path.js.out));
	}
	rebundle();
}
gulp.task('js', function (done) {
	compile();
	done();
});

gulp.task('css', function (done) {
	gulp.src(path.css.main)
	.pipe(stylus({ use: nib(), 'include css': true, compress: true }))
	.pipe(cleancss({keepSpecialComments: 0}))
	.pipe(gulp.dest(path.css.out));
	done();
});

gulp.task('img', gulp.series(function (done) {
	gulp.src(path.img.in)
		.pipe(newer(path.img.out))
		.pipe(imagemin({
			progressive: true,
			interlaced: true,
			multipass:true,
			svgoPlugins: [{removeViewBox: false}],
			use: [
				pngquant({quality: 70}),
				jpegoptim({progressive:true, max: 70})
			]
		}))
		.pipe(gulp.dest(path.img.out));
	done();
}));

gulp.task('obs', function (done) {
	gulp.watch([path.css.main, path.css.components, path.css.vendor], gulp.parallel('css'));
	gulp.watch(path.img.in, gulp.parallel('img'));
	compile(true);
	done();
});

gulp.task('sync', gulp.series('css','js','img'));

/* Tarea por defecto */
gulp.task('default', gulp.series('obs'));
