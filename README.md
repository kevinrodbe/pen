Proyecto boilerplate de @kevinrodbe
==============
Work enviroment.


# **Actualizar package**

> Ver diferencias entre las versiones de nuestros paquetes
```
ncu
```

> Actualizar package json
```
ncu -u
```

>instalar packages
```
npm install -d
```


## Si aparece el mensaje: ##
```
events.js:72
        throw er; // Unhandled 'error' event
              ^
Error: spawn ENOENT
    at errnoException (child_process.js:988:11)
    at Process.ChildProcess._handle.onexit (child_process.js:779:34)
```

Es porque no se instalaron correctamente las siguientes dependencias:


```
sudo apt-get install imagemagick
sudo apt-get install graphicsmagick
sudo npm install --save-dev gulp-image-resize
sudo npm install --save-dev gulp-imagemin
sudo npm install --save imagemin-jpegoptim
sudo npm install --save imagemin-pngquant
```