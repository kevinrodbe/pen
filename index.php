<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js ie6" lang="es">   <![endif]-->
<!--[if IE 7]>     <html class="no-js ie7" lang="es">   <![endif]-->
<!--[if IE 8]>     <html class="no-js ie8" lang="es">   <![endif]-->
<!--[if IE 9 ]>    <html class="no-js ie9" lang="es">   <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="" lang="es"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
	<title>Akuma.pe - Diseño y Desarrollo de Páginas Web</title>
	<link rel="stylesheet" href="dist/css/style.css">
</head>
<body>
<div class="mainPage">

</div>
<script src="dist/js/app.js"></script>
</body>
</html>
